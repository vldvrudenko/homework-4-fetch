const url = "https://swapi.dev/api/films/";
let table = document.getElementById("table_films");





fetch(url)
    .then((response) => response.json())
    .then((data) => {
        let films = data.results;

        for (let i = 0; i < films.length; i++) {

            var newRow = table.insertRow();

            var episodeCell = newRow.insertCell(0);
            var titleCell = newRow.insertCell(1);
            var crawlCell = newRow.insertCell(2);
            var charactersCell = newRow.insertCell(3);

            var episodeText = document.createTextNode(films[i].episode_id);
            var titleText = document.createTextNode(films[i].title);
            var crawlText = document.createTextNode(films[i].opening_crawl);
            
            episodeCell.appendChild(episodeText);
            titleCell.appendChild(titleText);
            crawlCell.appendChild(crawlText);

            


            console.log(films[i].title);
            let characters = films[i].characters;

            let characterPromises = [];

            for (let j = 0; j < characters.length; j++) {
                let promise = fetch(characters[i]);

                characterPromises.push(promise);
            }
            
            Promise.all(characterPromises)
                .then(responses => {
                    var charactersString = "";

                    for(let i = 0; i< responses.length; i++){
                        let result = responses[i].json();
                        result.then(data => {
                            charactersString = charactersString + " " + data.name;
                        });
                    };

                    var charactersText = document.createTextNode(charactersString);
                    charactersCell.appendChild(charactersText);
                });

        }
    });